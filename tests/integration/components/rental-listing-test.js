import { moduleForComponent, test } from 'ember-qunit';
import hbs from 'htmlbars-inline-precompile';
import EmberObject from '@ember/object';
import { run } from '@ember/runloop';

// Mock rental object for testing purposes.
const rental = EmberObject.create({
  image: 'fake.png',
  title: 'test-title',
  owner: 'test-owner',
  propertyType: 'test-type',
  city: 'test-city',
  bedrooms: 3,
});

moduleForComponent('rental-listing', 'Integration | Component | rental listing', {
  integration: true
});

test('should display rental details', function (assert) {
  // Assign rental to rentalObj in local scope.
  this.set('rentalObj', rental);
  // TODO: read about tagged template literals.
  // Render component.
  this.render(hbs`{{rental-listing rental=rentalObj}}`);
  // Test if listing heading is equal to 'test-title' (set in rental object).
  assert.equal(this.$('.listing h3').text(), 'test-title', 'Title: test-title');
  // Test if owner is equal to 'Owner: test-owner'.
  assert.equal(this.$('.listing .owner').text().trim(), 'Owner: test-owner', 'Owner: test-owner');
});

test('should toggle wide class on click', function (assert) {
  this.set('rentalObj', rental);
  this.render(hbs`{{rental-listing rental=rentalObj}}`);
  // Test if no images have the wide class.
  assert.equal(this.$('.image.wide').length, 0, 'initially rendered small');
  // Click the image.
  run(() => document.querySelector('.image').click());
  // Test if the image now has the wide class after clicking.
  assert.equal(this.$('.image.wide').length, 1, 'rendered wide after click');
  // Click the image again.
  run(() => document.querySelector('.image').click());
  // Test if no images have the wide class.
  assert.equal(this.$('.image.wide').length, 0, 'rendered small after second click');
});
